<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'FrontEnd'], function(){
    Route::get('homepage','HomepageController@index')->name('homeA');
    Route::get('blog','BlogController@list')->name('viewBlog');
    Route::get('blog/{id}','BlogController@detail')->name('detailBlog');
    Route::post('rate','RateController@rate')->name('rateBlog');
    Route::post('comment','BlogController@comment')->name('commentBlog');
    Route::resource('product','ProductController');
   
    Route::group(['namespace' => 'User','prefix'=>'member'], function(){
        Route::get('login','MemberController@getLogin')->name('viewLogin');
        Route::post('login','MemberController@login')->name('login');
        Route::get('register','MemberController@getRegister')->name('viewRegister');
        Route::post('register','MemberController@register')->name('register');
        Route::get('logout','MemberController@logout')->name('logout');
        Route::get('account','MemberController@getAccount')->name('viewAccount');
        Route::post('account','MemberController@update');
    });
});

// Route::get('father',function(){
//     return view('frontend.layout.father');
// });


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::group(['namespace' => 'Admin','prefix'=>'admin'], function(){
    Route::get('dashboard','HomeController@index');
    Route::get('profile','UserController@index')->name('show');
    // Route::post('edit/{id}','UserController@update');
    Route::post('profile','UserController@update');
    Route::resource('country', 'CountryController');
    Route::resource('blog', 'BlogController');
    Route::resource('brand','BrandController');
    Route::resource('category','CategoryController');
});


