@extends('admin.layout.father')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Basic Table</h4>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex align-items-center justify-content-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Basic Table</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card card-body">
                <h4 class="card-title">Country</h4>
                <h5 class="card-subtitle"> Country </h5>
                <form action="{{route('country.store')}}" method="POST" class="form-horizontal m-t-30">
                    @csrf
                    <div class="form-group">
                        <label><span class="help"> Name </span></label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    @if ($errors->has('name'))
                            <p class="help is-danger" style="color: red">Dien ten</p>
                    @endif
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection