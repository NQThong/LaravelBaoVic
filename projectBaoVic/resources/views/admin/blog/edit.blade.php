@extends('admin.layout.father')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Basic Table</h4>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex align-items-center justify-content-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Basic Table</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card card-body">
                <h4 class="card-title">Blog</h4>
                <h5 class="card-subtitle"> Blog </h5>
                @foreach ($data as $datas)
                    <form action="{{route('blog.update',$datas->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label><span class="help"> Title </span></label>
                            <input type="text" class="form-control" name="title" value="{{old('title', $datas->title)}}">
                        </div>
                        @if ($errors->has('title'))
                                <p class="help is-danger" style="color: red">Dien title</p>
                        @endif
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="2" name="description" >{{$datas->description}}</textarea>
                        </div>
                        @if ($errors->has('description'))
                                <p class="help is-danger" style="color: red">Dien description</p>
                        @endif
                        <div class="form-group">
                            <label>Content</label>
                            <textarea class="form-control" rows="5" name="content" id="demo">{{strip_tags($datas->content)}}</textarea>
                        </div>
                        @if ($errors->has('content'))
                                <p class="help is-danger" style="color: red">Dien content</p>
                        @endif
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" name="image" class="form-control">
                        </div>
                        @if ($errors->has('image'))
                                <p class="help is-danger" style="color: red">Up image</p>
                        @endif
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection