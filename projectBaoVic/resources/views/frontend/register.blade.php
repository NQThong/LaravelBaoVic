@extends('frontend.layout.father')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}
            <div class="col-sm-4">
                <div class="signup-form">
                    <h2>New User Signup!</h2>
                    <form method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="text" name="name" placeholder="Name"/>
                        @if ($errors->has('name'))
                            <p class="help is-danger" style="color: red">Dien ten</p>
                        @endif
                        <input type="email" name="email" placeholder="Email Address"/>
                        @if ($errors->has('email'))
                            <p class="help is-danger" style="color: red">Nhap email hoac email da ton tai</p>
                        @endif
                        <input type="password" name="password" placeholder="Password"/>
                        @if ($errors->has('password'))
                            <p class="help is-danger" style="color: red">Dien password</p>
                        @endif
                        <input type="text" name="phone" placeholder="Phone"/>
                        <input type="text" name="message" placeholder="Message"/>
                        Country:
                        <select name="country">
                            @foreach ($data as $datas)
                                <option value="{{$datas->id}}">{{$datas->name}}</option>
                            @endforeach
                        </select>
                        Image:
                        <input type="file" name="image">
                        @if ($errors->has('image'))
                            <p class="help is-danger" style="color: red">Ko phai image</p>
                        @endif
                        <button type="submit" class="btn btn-default">Dang ky</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection