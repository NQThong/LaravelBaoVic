@extends('frontend.layout.father')
@section('content')
<center>
    <div class="col-sm-5">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {!! session()->get('message') !!}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger">
            {!! session()->get('error') !!}
        </div>
        @endif
    </div>
</center>

<section id="form">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Login to your account</h2>
                    <form method="POST">
                        @csrf
                        <input type="text" name="email" placeholder="Email" />
                        @if ($errors->has('email'))
                            <p class="help is-danger" style="color: red">Dien email</p>
                        @endif
                        <input type="password" name="password" placeholder="Pass" />
                        @if ($errors->has('password'))
                            <p class="help is-danger" style="color: red">Dien Pass</p>
                        @endif
                        <span>
                            <input type="checkbox" name="remember_token" class="checkbox"> 
                            Keep me signed in
                        </span>
                        <button type="submit" class="btn btn-default">Login</button>
                    </form>
                </div><!--/login form-->
            </div>
        </div>
    </div>
</section>
@endsection