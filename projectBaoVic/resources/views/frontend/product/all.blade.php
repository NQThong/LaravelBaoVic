@extends('frontend.layout.father')
@section('id')
	cart_items
@endsection
@section('content')
<center>
    <div class="col-sm-4">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {!! session()->get('message') !!}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger">
            {!! session()->get('error') !!}
        </div>
        @endif
    </div>
</center>

<div class="table-responsive cart_info">
    <table class="table table-condensed">
        <thead>
            <tr class="cart_menu">
                <td class="image">ID</td>
                <td class="description">Name</td>
                <td class="price">Image</td>
                <td class="quantity">Price</td>
                <td class="total">Action</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="cart_product">
                    <a href=""><img src="images/cart/one.png" alt=""></a>
                </td>
                <td class="cart_description">
                    <h4><a href="">Colorblock Scuba</a></h4>
                </td>
                <td class="cart_price">
                    <p>$59</p>
                </td>
                <td class="cart_price">
                    <p>$59</p>
                </td>
                <td class="cart_total">
                    <p class="cart_total_price">Sua</p>
                    <p class="cart_total_price">Xoa</p>
                </td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
                <td colspan="2">
                    <table class="table table-condensed total-result">
                        <tbody><tr>
                            <td>
                                <a href="">Add New</a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection



{{--  --}}
{{-- if($request->hasfile('filename'))
{

    foreach($request->file('filename') as $image)
    {

        $name = $image->getClientOriginalName();
        $name_2 = "img40_".$image->getClientOriginalName();
        $name_3 = "img200_".$image->getClientOriginalName();

       
        
        $path = public_path('upload/product/' . $name);
        $path2 = public_path('upload/product/' . $name_2);
        $path3 = public_path('upload/product/' . $name_3);

        Image::make($image->getRealPath())->save($path);
        Image::make($image->getRealPath())->resize(50, 70)->save($path2);
        Image::make($image->getRealPath())->resize(200, 300)->save($path3);
        
        $data[] = $name;
    }
}

$product= new Products();
$product->filename=json_encode($data);
$product->save(); --}}
