@extends('frontend.layout.father')
@section('content')
<center>
    <div class="col-sm-4">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {!! session()->get('message') !!}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger">
            {!! session()->get('error') !!}
        </div>
        @endif
    </div>
</center>
{{-- <section id="form"><!--form-->
    <div class="container">
        <div class="row"> --}}
            <div class="col-sm-4">
                <div class="signup-form">
                    <h2>User Update!</h2>
                    <form method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="text" name="name" value="{{auth()->user()->name}}"/>
                        @if ($errors->has('name'))
                            <p class="help is-danger" style="color: red">Dien ten</p>
                        @endif
                        <input type="email" name="email" value="{{auth()->user()->email}}"/>
                        @if ($errors->has('email'))
                            <p class="help is-danger" style="color: red">Nhap email hoac email da ton tai</p>
                        @endif
                        <input type="password" name="password" value="{{auth()->user()->password}}"/>
                        @if ($errors->has('password'))
                            <p class="help is-danger" style="color: red">Dien password</p>
                        @endif
                        <input type="text" name="phone" value="{{auth()->user()->phone}}"/>
                        <input type="text" name="message" value="{{auth()->user()->message}}"/>
                        Country:
                        <select name="country">
                            @foreach ($data as $datas)
                                <option value="{{$datas->id}}">{{$datas->name}}</option>
                            @endforeach
                        </select>
                        Image:
                        <input type="file" name="image">
                        @if ($errors->has('image'))
                            <p class="help is-danger" style="color: red">Ko phai image</p>
                        @endif
                        <button type="submit" class="btn btn-default">Dang ky</button>
                    </form>
                </div>
            </div>
        {{-- </div>
    </div>
</section> --}}
@endsection