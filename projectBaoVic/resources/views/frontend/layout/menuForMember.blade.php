<div class="col-sm-3" id="a3">
    <div class="left-sidebar">
        <h2>Account</h2>
        <div class="panel-group category-products" id="accordian">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="{{route('viewAccount')}}">
                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                            Account
                        </a>
                    </h4>
                </div>
                {{-- <div id="sportswear" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Nike </a></li>
                            <li><a href="#">Under Armour </a></li>
                            <li><a href="#">Adidas </a></li>
                            <li><a href="#">Puma</a></li>
                            <li><a href="#">ASICS </a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="{{route('product.index')}}">
                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                            My Product
                        </a>
                    </h4>
                </div>
            </div> 
        </div>
    </div>
</div>
