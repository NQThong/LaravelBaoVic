<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('FE/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('FE/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('FE/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('FE/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('FE/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('FE/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('FE/css/responsive.css')}}" rel="stylesheet">      
    <link rel="shortcut icon" href="{{asset('FE/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('FE/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('FE/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('FE/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('FE/images/ico/apple-touch-icon-57-precomposed.png')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('FE/css/rate.css')}}">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</head><!--/head-->

<body>
	@include('frontend.layout.header')

	@php 
		$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$convertUrl = explode('/',(parse_url($url, PHP_URL_PATH)));
		// echo "<pre/>";
		// var_dump($convertUrl);
		if(empty($convertUrl[5])){
			$getNameView = $convertUrl[4];
		}else{
			$getNameView = $convertUrl[5];
		}
		// var_dump($getNameView);
	@endphp

	@if(($getNameView == 'homepage'))
	@include('frontend.layout.slider')
		<section id=@yield('id')>
			<div class="container">
				<div class="row">
					@include('frontend.layout.menuForCategory')
					@yield('content')
				</div>
			</div>
		</section>
	@endif

	@if(($getNameView == 'account') || ($getNameView == 'product'))
		<section id=@yield('id')>
			<div class="container">
				<div class="row">
					@include('frontend.layout.menuForMember')
					@yield('content')
				</div>
			</div>
		</section>
	@endif

	@if(($getNameView == 'login') || ($getNameView == 'cart') || ($getNameView == 'register'))
		<section id=@yield('id')>
			<div class="container">
				<div class="row">
					@yield('content')
				</div>
			</div>
		</section>
	@endif

	@if(($getNameView == 'blog'))
		<section id=@yield('id')>
			<div class="container">
				<div class="row">
					@include('frontend.layout.menuForCategory')
					@yield('content')
				</div>
			</div>
		</section>
	@endif

    @include('frontend.layout.footer')
	
    <script src="{{asset('FE/js/jquery.js')}}"></script>
	<script src="{{asset('FE/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('FE/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('FE/js/price-range.js')}}"></script>
    <script src="{{asset('FE/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('FE/js/main.js')}}"></script>
    <script src="{{asset('FE/js/jquery-1.9.1.min.js')}}"></script>
    <script>
        if(screen.width <= 736){
            document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
        }
    </script>
        <script>
			$(document).ready(function(){
				//vote
				$('.ratings_stars').hover(
					// Handles the mouseover
					function() {
						$(this).prevAll().andSelf().addClass('ratings_hover');
						// $(this).nextAll().removeClass('ratings_vote'); 
					},
					function() {
						$(this).prevAll().andSelf().removeClass('ratings_hover');
						// set_votes($(this).parent());
					}
				);
				
				//Rate
				$('.ratings_stars').click(function(){
					var Values =  $(this).find("input").val();
					loggedIn = "{{ Auth::check() }}";
					blogId = $("#blog_id").val();
					token = $('input[name="_token"]').val();

					if(loggedIn == ''){
						alert('Vui long dang nhap');
					}else{
						userId = "{{ Auth::id() }}";
						// console.log(userId);

						$.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
                    	});
					
						$.ajax({
							method: "POST",
							url: '{{route('rateBlog')}}',
							data: {
								token, blogId, userId, Values
							}
							
						});
					}
			
					// alert(Values);
					if ($(this).hasClass('ratings_over')) {
						$('.ratings_stars').removeClass('ratings_over');
						$(this).prevAll().andSelf().addClass('ratings_over');
					} else {
						$(this).prevAll().andSelf().addClass('ratings_over');
					}
					// e.preventDefault();
				});

				//Cmt
				$('.btnCmt').click(function(e){
					loggedIn = "{{ Auth::check() }}";

					if(loggedIn == ''){
						alert('Vui long dang nhap');
						e.preventDefault();
					}else{
						if(!$(".comment").val()){
							alert('Nhap binh luan');
							e.preventDefault();
						}else{
							$('.btnCmt').submit();
						}
					}
				});

				//Replay
				$('.btnReplay').click(function(){
					id = $(this).closest(".cmt123").find(".idCmtFather").val();
					$(".parent_id").val(id);
					// console.log(id);
				});

			});
		</script>
</body>
</html>