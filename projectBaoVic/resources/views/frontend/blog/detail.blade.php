@extends('frontend.layout.father')
@section('content')
<div class="col-sm-9">
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        <div class="single-blog-post">
            <h3>{{$blog['title']}}</h3>
            @php
                $tachTime = explode(' ',$blog['created_at']);
            @endphp
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-clock-o"></i> {{$tachTime[1]}}</li>
                    <li><i class="fa fa-calendar"></i> {{$tachTime[0]}}</li>
                </ul>
            </div>
            <a href="">
                <img src="{{asset('upload/blog/'.$blog['image'])}}" alt="">
            </a>
            <p>
                {{strip_tags($blog['content'])}}
            </p> 
            <br>
            <div class="pager-area">
                <ul class="pager pull-right">
                    @if ($previous != NULL)
                        <li><a href="{{ URL('blog/'.$previous)}}">Pre</a></li>
                    @endif
                    @if ($next != NULL)
                        <li><a href="{{ URL('blog/'.$next)}}">Next</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    
    <div class="rating-area">
        <div class="rate">
            <div class="vote">
                {{-- @if (($lamTron == 0)) 

                @else
                    TBC danh gia
                    @php
                        $lamTron = round($sumStar/$countRate)    
                    @endphp
                    @for ($i = 1; $i <= $lamTron; $i++)
                        <div class="star_{{$i}} ratings_stars ratings_hover ratings_over"><input type="hidden" value="{{$i}}"></div>
                    @endfor
                @endif --}}
                <form>
                    @csrf
                    @if (Auth::check())
                        <?php Auth::check() ?>
                    @else
                        <?php Auth::check() ?>
                    @endif
                    <input type="hidden" id="blog_id" value="{{$blog['id']}}">
                    
                            @for ($i = 1; $i <= 5; $i++)
                                @if ($i <= $lamTron)
                                    <div class="star_{{$i}} ratings_stars ratings_hover ratings_over"><input type="hidden" value="{{$i}}"></div>
                                @else
                                    <div class="star_{{$i}} ratings_stars"><input type="hidden" value="{{$i}}"></div>
                                @endif
                            @endfor
                        
                </form>
            </div> 
        </div>
    </div>
    

    <div class="socials-share">
        <a href=""><img src="images/blog/socials.png" alt=""></a>
    </div>

    <div class="response-area">
        @if ((count($comment)) == 0)
            <h2>0 RESPONSES</h2>
        @else
            <h2>{{count($comment)}} RESPONSES</h2>
            <ul class="media-list">
            @foreach ($comment as $comments)
                @php
                    $time = $comments->created_at;
                    $cvTime = explode(' ',$time);   
                @endphp
                {{-- Phan Cha --}}
                <li class="media">				
                    <a class="pull-left" href="#">
                        <img class="media-object" src="{{asset('upload/customer/'.$comments->user->image)}}" alt="">
                    </a>
                    <div class="media-body cmt123">
                        <ul class="sinlge-post-meta">
                            <li><i class="fa fa-user"></i>{{ $comments->user->name }}</li>
                            <li><i class="fa fa-clock-o"></i> {{$cvTime[1]}}</li>
                            <li><i class="fa fa-calendar"></i> {{$cvTime[0]}}</li>
                        </ul>
                        <p>{{$comments->content}}</p>
                        <input type="hidden" class="idCmtFather" value="{{$comments->id}}">
                        <a class="btn btn-primary btnReplay"><i class="fa fa-reply"></i>Replay</a>
                    </div>
                </li>
                {{-- Phan Con --}}
                @foreach ($commentNotNull as $miniCmt)
                    @if ($comments->id == $miniCmt->parent_id)
                        @php
                            $time = $miniCmt->created_at;
                            $cvTime = explode(' ',$time);   
                        @endphp
                        <li class="media second-media">
                            <a class="pull-left">
                                <img class="media-object" src="{{asset('upload/customer/'.$miniCmt->user->image)}}" alt="">
                            </a>
                            <div class="media-body cmt123">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>{{ $miniCmt->user->name }}</li>
                                    <li><i class="fa fa-clock-o"></i> {{$cvTime[1]}}</li>
                                    <li><i class="fa fa-calendar"></i> {{$cvTime[0]}}</li>
                                </ul>
                                <p>{{$miniCmt->content}}</p>
                                <input type="hidden" class="idCmtFather" value="{{$miniCmt->id}}">
                                <a class="btn btn-primary btnReplay"><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li>
                    @endif
                @endforeach
            @endforeach
            </ul>
        @endif					
    </div>

    <div class="replay-box">
        <div class="row">
            <div class="col-sm-12">
                <h2>Leave a replay</h2>
                
                <div class="text-area">
                    <form action="{{route('commentBlog')}}" method="POST">
                        @csrf
                        <div class="blank-arrow">
                            <label>
                                @auth {{auth()->user()->name}} 
                                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                                @else 
                                    Your Name 
                                @endauth
                            </label>
                        </div>
                        <span>*</span>
                            <input type="hidden" name="parent_id" class="parent_id" value="">
                            <input type="hidden" name="blog_id" value="{{$blog['id']}}">
                            <textarea name="content" class="comment" rows="11"></textarea>
                            <button type="submit" class="btn btn-primary btnCmt">post comment</button>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/Repaly Box-->
</div>	
@endsection