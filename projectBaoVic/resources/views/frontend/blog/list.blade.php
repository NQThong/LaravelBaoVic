@extends('frontend.layout.father')
@section('content')
<div class="col-sm-9">
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        @foreach ($blog as $blogs)
        @php
            $tachTime = explode(' ',$blogs->created_at);
        @endphp
        <div class="single-blog-post">
            <h3>Girls Pink T Shirt arrived in store</h3>
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-clock-o"></i> {{ $tachTime[1] }}</li>
                    <li><i class="fa fa-calendar"></i> {{ $tachTime[0] }}</li>
                </ul>
                <span>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                </span>
            </div>
            <a href="">
                <img src="{{asset('upload/blog/'.$blogs->image)}}" alt="">
            </a>
            <p>{{$blogs->description}}</p>
            <p>{{strip_tags($blogs->content)}}</p>
            <a  class="btn btn-primary" href="{{route('detailBlog',$blogs->id)}}">Read More</a>
        </div>
        @endforeach
        
        <div class="pagination-area">
            {{ $blog->links() }}
        </div>
    </div>
</div>
@endsection