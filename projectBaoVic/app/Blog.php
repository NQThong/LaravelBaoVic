<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    // public $timestamps = false; 
    protected $fillable = [
    	'title', 'image', 'description', 'content'
    ];
    protected $primaryKey = 'id';
 	protected $table = 'blog';
}
