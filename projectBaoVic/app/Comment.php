<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
    	'content', 'parent_id', 'blog_id', 'user_id'
    ];
    protected $primaryKey = 'id';
 	protected $table = 'comment';

    public function user()
    {
        return $this->belongsTo('App\User');
    } 
}
