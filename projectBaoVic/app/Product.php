<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    	'name'
    ];
    protected $primaryKey = 'id';
 	protected $table = 'country';
}
