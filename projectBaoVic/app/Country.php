<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false; 
    protected $fillable = [
    	'name'
    ];
    protected $primaryKey = 'id';
 	protected $table = 'country';
}
