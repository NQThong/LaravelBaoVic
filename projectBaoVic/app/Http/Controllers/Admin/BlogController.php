<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BackEnd\BlogRequest;
use App\Blog;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Blog::all();
        return view('admin.blog.all',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $insert = new Blog;
        $insert->title = $request->title;
        $insert->description = $request->description;
        $insert->content = $request->content;
        $img = $request->image;
        if($img){
            $getNameImg = $img->getClientOriginalName();
            $user->image = $getNameImg;
        }
        $insert->save();
        if($insert->save()){
            if($img != null){
                $img->move('upload/blog',$getNameImg);
            }
            return redirect()->route('blog.index')->with('message','Them blog t.cong');
        }else{
            return redirect()->route('blog.index')->with('error','Them blog t.bai');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $data = Blog::findorfail($id);
        $data = Blog::where('id',$id)->get();
        return view('admin.blog.edit',compact('data'));
        // dd($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        $data = Blog::findorfail($id);
        $image = $request->file('image');
        $update = $request->all();
        $getNameImg = $image->getClientOriginalName();
        $update['image'] = $getNameImg;
        if($data->update($update)){
            $image->move('upload/blog',$getNameImg);
            return redirect()->route('blog.index')->with('message','Cap nhat blog t.cong');
        }else{
            return redirect()->route('blog.index')->with('error','Cap nhat blog t.bai');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Blog::findorfail($id);
        $user->delete();
        return redirect()->route('blog.index')->with('message','Xoa blog t.cong');
    }
}
