<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\BackEnd\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Country;
use Auth; 
// use Illuminate\Support\Facades\Input;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $getDataUser = User::all();
        $getDataCountry = Country::all();
        // dd($getDataUser);
        return view('admin.profile',compact('getDataUser','getDataCountry'));
    }

    public function update(UserRequest $request){
        $userId = Auth::id();
        $getUserById = User::findOrFail($userId);
        $image = $request->image;
        $update = $request->all();
        
        if($request->password == null){
            $update['password'] = $getUserById->password;
        }else{
            if(!Hash::check($request->password, $getUserById->password)){
                $update['password'] = Hash::make($request->password);
            }else{
                // $msgPass = 'Ban da nhap pass cu';
                $update['password'] = $getUserById->password;
            }
        }

        if($image){
            $getNameImg = $image->getClientOriginalName();
        }

        if($getUserById->update($update)){
            if($image != null){
                $image->move('upload/admin',$getNameImg);
                $update['image'] = $getNameImg;
            }
            return redirect()->route('show')->with('message','Cap nhat profile t.cong');
        }else{
            return redirect()->route('show')->with('error','Cap nhat profile t.bai');
        }


        // try {
        //     $data = $request->validated();
        //     $data['image'] = $data['image']->store('users');
        //     $user->update($data);
        //     return redirect()->route('show')->with('message','Cap nhat profile t.cong');
        // } catch (\Throwable $th) {
        //     return redirect()->route('show')->with('message','Cap nhat profile t.cong');
        // }
        // $user = User::findOrFail($id);
        // User::where()
        // $oldPw = $getUserById->password;
        // $value = Crypt::decrypt($oldPw);
        // dd($value);
        // $image = $request->image;
        // $update = $request->all();
        // $data = $request->validated();
        // $data['image'] = $data['image']->store('users');
        // dd($data);
        
        // if(!$data['password']) unset($data['password']);
        // $user->update($data);
            // dd($request->validated());
            //    $data = $request->validated();
            // $data['image'] = $data['image']->store('users');
            // if(!Hash::check($request->password, $getUserById->password)){
            //     $update['password'] = Hash::make($request->password);
            // }

            // if($update['password']){
            //     $update['password'] = bcrypt($data['password']);
            // }else{
            //     $update['passoword'] = $getUserById->password;
            // }

        

        
    }
}

