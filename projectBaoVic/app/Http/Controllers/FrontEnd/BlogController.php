<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use App\Rate;
use App\Comment;
use App\User;
use Auth;

class BlogController extends Controller
{
    public function list(){
        // $blog = Blog::orderBy('created_at', 'desc')->paginate(3);
        $blog = Blog::latest()->paginate(3);
        // dd($blog);
        return view('frontend.blog.list',compact('blog'));
    }

    public function detail($id){
        $blog = Blog::findorfail($id);
        $previous = Blog::where('id', '<', $blog->id)->max('id');
        $next = Blog::where('id', '>', $blog->id)->min('id');
        $rate = Rate::where('blog_id',$blog->id)->count();
        $sumStar = Rate::where('blog_id',$blog->id)->sum('star');
        $comment = Comment::where('blog_id',$blog->id)->where('parent_id',NULL)->get();
        $commentNotNull = Comment::where([
            ['blog_id', '=', $blog->id],
            ['parent_id', '!=', NULL]
        ])->get();
        // dd($comment['0']->user_id);
        // dd($commentNotNull);
        $lamTron = 0;

        if($rate > 0){
            $lamTron = round($sumStar/$rate);
        }

        return view('frontend.blog.detail',compact('blog','previous','next','lamTron','comment','commentNotNull'));

    }

    public function comment(Request $request){
        $data = $request->all();
        $insert = Comment::create($data);
        return redirect()->back();
        // dd($insert);
    }
}
