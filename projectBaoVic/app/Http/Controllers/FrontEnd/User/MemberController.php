<?php

namespace App\Http\Controllers\FrontEnd\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Country;
use App\Http\Requests\BackEnd\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\FrontEnd\LoginRequest;


class MemberController extends Controller
{
    public function getLogin(){
        return view('frontend.login');
    }

    public function login(LoginRequest $request){
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0
        ];
        
        $remember = false;
        
        if($request->remember_token){
            // dd($request->remember_token);
            $remember = true;
        }

        if(Auth::attempt($credentials,$remember)){
            // dd($remember);
            return redirect()->route('homeA');
        }else{
            // dd($remember);
            // dd(Auth::attempt($credentials,$remember));
            return redirect()->back()->with('error','Sai tk or mk');
        }
    }

    public function getRegister(){
        $data = Country::all();
        return view('frontend.register',compact('data'));
    }

    public function register(UserRequest $request){
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->level = 0;
        $user->password = Hash::make($request->password);
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->message = $request->message;
        $img = $request->image;
        if($img){
            $getNameImg = $img->getClientOriginalName();
            $user->image = $getNameImg;
        }
        $user->save();
        if($user->save()){
            if($img != null){
                $img->move('upload/customer',$getNameImg);
            }
            return redirect()->route('viewLogin')->with('message','DK t.cong');
        }else{
            return redirect()->route('viewLogin')->with('error','DK t.bai');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('viewLogin'); 
    }

    public function getAccount(){
        if(Auth::check()){
            $data = Country::all();
            return view('frontend.account.account',compact('data'));
        }else{
            return redirect()->route('viewLogin');
        }
        
    }

    public function update(UserRequest $request){
        $id = Auth::id();
        $user = User::findorfail($id);
        $img = $request->image;
        $data = $request->all();
        if($img){
            $getNameImg = $img->getClientOriginalName();
            $data['image'] = $getNameImg;
        }

        if($data['password']){
            $data['password'] = Hash::make($data['password']);
        }else{
            $data['password'] = $user->password;
        }

        $update = $user->update($data);
        if($update){
            if($img != null){
                $img->move('upload/customer',$getNameImg);
            }
            return redirect()->route('viewAccount')->with('message','Cap nhat t.cong');
        }else{
            return redirect()->route('viewAccount')->with('error','Cap nhat t.bai');
        }
    }

}
