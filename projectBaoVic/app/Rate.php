<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
    	'star', 'blog_id', 'user_id'
    ];
    protected $primaryKey = 'id';
 	protected $table = 'rate';
}
