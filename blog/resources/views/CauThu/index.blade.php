<!DOCTYPE html>
<html>
    <head>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <meta charset="utf-8">
        <style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
        </style>
    </head>
    <body>
        <table id="datatable" style="border: 1px solid">
            <center>
                <div class="col-sm-5">
                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {!! session()->get('message') !!}
                    </div>
                    @elseif(session()->has('error'))
                    <div class="alert alert-danger">
                        {!! session()->get('error') !!}
                    </div>
                    @endif
                </div>
            </center>
            <h1>Quản lý cầu thủ</h1>
            
            <thead>
                <tr role="row">
                    <th>ID</th>
                    <th>Tên cầu thủ</th>
                    <th>Tuổi</th>
                    <th>Quốc tịch</th>
                    <th>Vị trí</th>
                    <th>Lương</th>
                    <th>Img</th>
                    <th>Anh</th>
                    <th style="width: 7%;">Edit</th>
                    <th style="width: 10%;">Delete</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                @foreach ($getAllPlayer as $getAllPlayers)
                <tr>
                    <td>{{$loop->iteration}} </td>
                    <td>{{$getAllPlayers->name}}</td>
                    <td>{{$getAllPlayers->age}}</td>
                    <td>{{$getAllPlayers->national}}</td>
                    <td>{{$getAllPlayers->position}}</td>
                    <td>${{$getAllPlayers->salary}}</td>
                    <td>{{$getAllPlayers->image}}</td>
                    <td><img src="{{asset('upload/'.$getAllPlayers->image)}}"  style="width:100px; height:100px"></td>
                    <td>
                        {{-- Query Builder --}}
                        {{-- <a href="{{URL('football/edit/'.$getAllPlayers->id)}}">
                        Sua
                        </a> --}}
                        {{-- Eloquent --}}
                        <a href="{{route('football.edit',$getAllPlayers->id)}}">
                            Sua
                        </a>
                    </td>
                    <td>
                        {{-- <a href="{{URL('football/delete/'.$getAllPlayers->id)}}" onclick="return confirm('Are you sure?')" ui-toggle-class="" >
                            Xoa
                        </a> --}}
                        {{-- <a href="{{route('football.destroy',$getAllPlayers->id)}}" data-method="delete" onclick="return confirm('Are you sure?')">
                            Xoa
                        </a> --}}
                        <form action="{{route('football.destroy',$getAllPlayers->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" onclick="return confirm('Are you sure?')">Xoa</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                <td colspan="12">
                    <a href="{{route('football.create')}}"><button id="button">Thêm cầu thủ</button></a>
                </td>
            </tfoot>
        </table>
    </body>
</html>