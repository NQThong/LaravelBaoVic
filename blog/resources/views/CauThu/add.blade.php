{{-- @extends('layoutMsg.errors') --}}

<form action="{{route('football.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif --}}
    Ten: <input type="text" name="name">
    </br>
    @if ($errors->has('name'))
        <p class="help is-danger" style="color: red">Dien ten</p>
    @endif
    Tuoi: <input type="text" name="age">
    </br>
    @if ($errors->has('age'))
        <p class="help is-danger" style="color: red">Dien tuoi</p>
    @endif
    Quoc tich: <input type="text" name="national">
    </br>
    @if ($errors->has('national'))
        <p class="help is-danger" style="color: red">Dien quoc tich</p>
    @endif
    Vi tri: <input type="text" name="position">
    </br>
    @if ($errors->has('position'))
        <p class="help is-danger" style="color: red">Dien vi tri</p>
    @endif
    Luong: <input type="text" name="salary">
    </br>
    @if ($errors->has('salary'))
        <p class="help is-danger" style="color: red">Dien luong</p>
    @endif
    Anh: <input type="file" name="image">
    </br>
    @if ($errors->has('image'))
        <p class="help is-danger" style="color: red">Up anh</p>
    @endif
    <button type="submit">Send</button>
</form>