@foreach ($getPlayerById as $getPlayerByIds)
    <form action="{{route('football.update',$getPlayerByIds->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        Ten: <input type="text" name="name" value="{{ old('name', $getPlayerByIds->name) }}">
        </br>
        @if ($errors->has('name'))
            <p class="help is-danger" style="color: red">Dien ten</p>
        @endif
        Tuoi: <input type="text" name="age" value="{{ old('age', $getPlayerByIds->age) }}">
        </br>
        @if ($errors->has('age'))
            <p class="help is-danger" style="color: red">Dien tuoi</p>
        @endif
        Quoc tich: <input type="text" name="national" value="{{ old('national', $getPlayerByIds->national) }}">
        </br>
        @if ($errors->has('national'))
            <p class="help is-danger" style="color: red">Dien quoc tich</p>
        @endif
        Vi tri: <input type="text" name="position" value="{{ old('position', $getPlayerByIds->position) }}">
        </br>
        @if ($errors->has('position'))
            <p class="help is-danger" style="color: red">Dien vi tri</p>
        @endif
        Luong: <input type="text" name="salary" value="{{ old('salary', $getPlayerByIds->salary) }}">
        </br>
        @if ($errors->has('salary'))
            <p class="help is-danger" style="color: red">Dien luong</p>
        @endif
        Anh: <input type="file" name="image">
        </br>
        @if ($errors->has('image'))
            <p class="help is-danger" style="color: red">Up anh</p>
        @endif
        <button type="submit">Send</button>
    </form>
@endforeach
