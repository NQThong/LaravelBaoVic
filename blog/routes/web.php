<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('FE.homepage');
// });

// Route::get('cart', function () {
//     return view('FE.cart');
// });

Route::get('/','PageController@redirectHomepage')->name('home');
Route::get('home','PageController@redirectHomepage2')->name('home');
Route::get('cart','CartController@redirectCart')->name('cart');
Route::get('login','LoginController@redirectLogin')->name('login');
Route::get('checkout','CheckoutController@redirectCheckout')->name('checkout');

// Route::get('insertUser','UserController@insert')->name('insert');
// Route::get('showUser','UserController@show')->name('show');

//Quan li cau thu
// Route::group(['prefix' => 'football'], function () {
//     Route::get('show','FootballController@show')->name('show');
//     Route::get('add','FootballController@add')->name('add');
//     Route::post('insert','FootballController@insert')->name('insert');
//     Route::get('edit/{id}','FootballController@edit')->name('edit');
//     Route::post('update/{id}','FootballController@update')->name('update');
//     Route::get('delete/{id}','FootballController@delete')->name('delete');
// });

// Route::group(['prefix' => 'football'], function () {
// Route::resource('football', 'FootballController2');
// Route::resource('football', 'FootballController2')->only([
//     'index', 'create', 'store', 'edit', 'update', 'destroy'
// ]);
Route::resource('football', 'FootballController2')->except([
    'show'
]);
    // Route::get('show','FootballController2@index')->name('show');
    // Route::get('add','FootballController@add')->name('add');
    // Route::post('insert','FootballController@insert')->name('insert');
    // Route::get('edit/{id}','FootballController@edit')->name('edit');
    // Route::post('update/{id}','FootballController@update')->name('update');
    // Route::get('delete/{id}','FootballController@delete')->name('delete');
// });

