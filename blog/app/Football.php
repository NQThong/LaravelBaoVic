<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Football extends Model
{
    public $timestamps = false;
    protected $table = 'player';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'name','age','national','position','salary','image'
    ];
}
