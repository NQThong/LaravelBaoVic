<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FootballRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'age' => 'required',
            'national' => 'required',
            'position' =>'required',
            'salary' =>'required',
            'image' => 'required|image|mimes:jpeg,jpg,png,gif|required|max:10000'
        ];
    }
}
