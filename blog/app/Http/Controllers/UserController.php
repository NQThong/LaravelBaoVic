<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 
use Illuminate\Support\Facades\Redirect;
use Session;

class UserController extends Controller
{
    public function insert(){
        // $insertUser = DB::table('user')->insert([
        //     'name' => 'BaoVic',
        //     'age' => '123',
        //     'phone' => '123456789'
        // ]);

        $insertUser = DB::table('user')->insert([
            'name' => 'Thong',
            'age' => '88',
            'phone' => '456'
        ]);
        // if($insertUser == true){
        return view('FE.insertUser')->with('msg','Insert thanh cong');
        // return redirect()->route('insert')->with('message','Insert thanh cong');
        // }else{
        //     return view('FE.insertUser')->with('error','Insert ko thanh cong');
        // }
    }

    public function show(){
        // $showUser = DB::table('user')->get();
        // $showUser = DB::table('user')->select('name','age')->get();
        // $showUser = DB::table('user')->select('name','age')->where('age',>,'100')->get();
        return view('FE.showUser',compact('showUser'));
    }
}
