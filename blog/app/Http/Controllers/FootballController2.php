<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FootballRequest;
use App\Football;

class FootballController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getAllPlayer = Football::all();
        // dd($getAllPlayer);
        return view('CauThu.index',compact('getAllPlayer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('CauThu.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FootballRequest $request)
    {
        $image = $request->file('image');
        // dd($image);
        if($image){
            $insert = new Football;
            $getNameImg = $image->getClientOriginalName();
            $image->move('upload',$getNameImg);

            $insert->image =  $getNameImg;
            $insert->name = $request->name;
            $insert->age = $request->age;
            $insert->national = $request->national;
            $insert->position = $request->position;
            $insert->salary = $request->salary;
            $insert->save();
            // dd($insert);
            return redirect()->route('football.index')->with('message','Them cau thu t.cong');
        }
        // if($request->file('image')){
        //     $insert = new Football;
        // }
        // $insert = new Football;
        // $insert->name = $request->
        // dd($insert);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getPlayerById = Football::find($id);
        // $getPlayerById = Football::where('id',$id)->get();
        // dd($getPlayerById);
        return view('CauThu.edit',compact('getPlayerById'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FootballRequest $request, $id)
    {
        $getPlayerById = Football::find($id);
        // $getPlayerById = Football::where('id',$id)->get();
        $image = $request->file('image');
        $insert = $request->all();
        // dd($insert);
        if($image){
            $getNameImg = $image->getClientOriginalName();
            $image->move('upload',$getNameImg);
            $insert['image'] =  $getNameImg;
            $insert['name'] = $request->name;
            $insert['age'] = $request->age;
            $insert['national'] = $request->national;
            $insert['position'] = $request->position;
            $insert['salary'] = $request->salary;
            $getPlayerById->update($insert);
            // dd($getPlayerById);
            return redirect()->route('football.index')->with('message','Cap nhat cau thu t.cong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $getPlayerById = Football::find($id);
        Football::destroy($id);
        return redirect()->route('football.index')->with('message','Xoa cau thu t.cong');
    }
}
